let screenVal = document.querySelector("#screen");
screenVal.value = 0;

let calculus = document.querySelector("#screenSm");
let recalc = false;

//#region addNumbers
let number = document.querySelectorAll('[data-number]');
number.forEach(btn => {
    btn.addEventListener('click', () => {
        updateScreen(btn.innerText)
        updateTop(false);
        if (recalc) {
            screenVal.value = btn.innerText;
            recalc = false;
        }
    })
})
//#endregion

let dot = document.querySelector("[data-dot]");
dot.addEventListener('click', () => {
    screenVal.value === '0' ? updateScreen(`0${dot.innerText}`) : updateScreen(dot.innerText)
})


//#region addSign
let sign = document.querySelectorAll('[data-sign]');
sign.forEach(btn => {
    btn.addEventListener('click', () => {
        if (screenVal.value == ' - ') {
            screenVal.value = 0;
        }
        if (screenVal.value.slice(-1) == ' ') {
            screenVal.value = screenVal.value.slice(0, -3);
        }

        if (btn.innerText == '-') {
            updateScreen(` ${btn.innerText} `)
        }
        else if (screenVal.value != 0) {
            updateScreen(` ${btn.innerText} `)
        }
        updateTop(false);
        recalc = false;
    })
})
//#endregion

//#region del

let del = document.querySelector('#del');
del.addEventListener('click', () => {
    if (screenVal.value.slice(-1) == ' ') {
        screenVal.value == 0 ? screenVal.value = 0 : screenVal.value = screenVal.value.slice(0, -3);
    } else {
        screenVal.value == 0 ? screenVal.value = 0 : screenVal.value = screenVal.value.slice(0, -1);
    }
    screenVal.value.length == 0 ? screenVal.value = 0 : screenVal.value = screenVal.value;
    if (recalc) {
        screenVal.value = 0;
        calculus.value = '';
        recalc = false;
    }
})

//#endregion

//#region clear
let clear = document.querySelector('#clear');
clear.addEventListener('click', () => {
    screenVal.value = 0;
    calculus.value = '';
})
//#endregion

//#region equal
let equal = document.querySelector('#equal');
equal.addEventListener('click', () => {
    if (screenVal.value.match(/\d?.?\d+\ [\+\-\x\÷] +\d?.?\d+/g)) {
        updateTop(true);
        calc(screenVal.value)
    }
})
//#endregion

function updateScreen(value) {
    screenVal.value === '0' ? screenVal.value = value : screenVal.value += value;
}

function updateTop(isEqual) {
    isEqual ? calculus.value = screenVal.value : calculus.value = '';
}

function calc(data) {
    recalc = true;
    let fixMultiply = data.replace(/x/g, '*');
    let fixDivide = fixMultiply.replace(/÷/g, '/');
    let result = eval(fixDivide).toPrecision(12);
    screenVal.value = parseFloat(result);
    if (screenVal.value.includes("Infinity")) screenVal.value = 'Error'
}

